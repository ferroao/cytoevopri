<!-- README.md is generated from README.Rmd. Please edit that file -->
<!-- badges: start -->
<!-- badges: end -->

<img src=www/logo.png align="right" width="12%" hspace="50">

# CytoEvo<br></br><br></br> Shiny App for Modelling the Evolution of Cytogenetic Characters <br></br><br></br>

<!-- <img src='www/logo.png' align="right" height="240" /> -->

This app models the evolution of cytogenetic characters.

Web-page: <https://cyto.shinyapps.io/cytoevo/>

First, go to the left side menu, and open each of the tabs:

-   Simulate
    -   Tree simulation
    -   Chromosome number simulation
    -   45S simulation
    -   Genome size simulation
    -   Interchromosomal asymmetry CVCL
    -   Summary table
    -   Variation explained by phylogenetic structure <br></br>

Then, go to the second left submenu

-   Index
    -   Import data and configure index
    -   Index Plot <br></br>

For large trees, you have to modify memory in the left menu:

-   Memory

How to use? summary

1.  Generate a tree in first tab, button A
2.  Pass through each tab to simulate characteres (semiautomatic)
3.  In the last tab the analysis PVR is done based on ([Serbin *et al.*,
    2019](#ref-Serbin_2019))

### References

Attali D. 2021. *Shinyjs: Easily improve the user experience of your
shiny apps in seconds*. <https://deanattali.com/shinyjs/>

Bailey E. 2022. *shinyBS: Twitter bootstrap components for shiny*.
<https://ebailey78.github.io/shinyBS>

Bolker B, Warnes GR, Lumley T. 2022. *Gtools: Various r programming
tools*. <https://github.com/r-gregmisc/gtools>

Chang W, Borges Ribeiro B. 2021. *Shinydashboard: Create dashboards with
shiny*. <http://rstudio.github.io/shinydashboard/>

Chang W, Cheng J, Allaire J, Sievert C, Schloerke B, Xie Y, Allen J,
McPherson J, Dipert A, Borges B. 2023. *Shiny: Web application framework
for r*. <https://CRAN.R-project.org/package=shiny>

Dowle M, Srinivasan A. 2023. *Data.table: Extension of ‘data.frame‘*.
<https://CRAN.R-project.org/package=data.table>

Glur C. 2020. *Data.tree: General purpose hierarchical data structure*.
<http://github.com/gluc/data.tree>

Hagen O, Stadler T. 2020. *TreeSimGM: Simulating phylogenetic trees
under general bellman harris and lineage shift model*.
<https://CRAN.R-project.org/package=TreeSimGM>

Harmon L, Pennell M, Brock C, Brown J, Challenger W, Eastman J, FitzJohn
R, Glor R, Hunt G, Revell L, Slater G, Uyeda J, Weir J, team C. 2023.
*Geiger: Analysis of evolutionary diversification*.
<https://CRAN.R-project.org/package=geiger>

Paradis E, Blomberg S, Bolker B, Brown J, Claramunt S, Claude J, Cuong
HS, Desper R, Didier G, Durand B, Dutheil J, Ewing R, Gascuel O,
Guillerme T, Heibl C, Ives A, Jones B, Krah F, Lawson D, Lefort V,
Legendre P, Lemon J, Louvel G, Marcon E, McCloskey R, Nylander J,
Opgen-Rhein R, Popescu A-A, Royer-Carenzi M, Schliep K, Strimmer K, de
Vienne D. 2023. *Ape: Analyses of phylogenetics and evolution*.
<https://CRAN.R-project.org/package=ape>

Revell LJ. 2023. *Phytools: Phylogenetic tools for comparative biology
(and other things)*. <https://github.com/liamrevell/phytools>

Schliep K, Paradis E, de Oliveira Martins L, Potts A, Bardel-Kahr I.
2023. *Phangorn: Phylogenetic reconstruction and analysis*.
<https://CRAN.R-project.org/package=phangorn>

Serbin GM, Machado RM, Pinto RB, Filho JAFD, Azevedo Tozzi AMG de,
Forni-Martins ER, Freitas Mansano V de. 2019. Karyological traits
related to phylogenetic signal and environmental conditions within the
hymenaea clade (leguminosae, detarioideae) *Perspectives in Plant
Ecology, Evolution and Systematics*Elsevier BV, 39: 125462.
<https://doi.org/10.1016/j.ppees.2019.125462>

Slowikowski K. 2023. *Ggrepel: Automatically position non-overlapping
text labels with ggplot2*. <https://github.com/slowkow/ggrepel>

Vu VQ. 2011. *Ggbiplot: A ggplot2 based biplot*.
<http://github.com/vqv/ggbiplot>

Wickham H. 2023. *Pryr: Tools for computing on the language*.
<https://github.com/hadley/pryr>

Wickham H, François R, Henry L, Müller K, Vaughan D. 2023. *Dplyr: A
grammar of data manipulation*.
<https://CRAN.R-project.org/package=dplyr>

Wickham H, Seidel D. 2022. *Scales: Scale functions for visualization*.
<https://CRAN.R-project.org/package=scales>

Xie Y, Cheng J, Tan X. 2023. *DT: A wrapper of the JavaScript library
DataTables*. <https://github.com/rstudio/DT>

Yu G, Lam TT-Y, Xu S. 2023. *Ggtree: An r package for visualization of
tree and annotation data*. <https://doi.org/10.18129/B9.bioc.ggtree>
